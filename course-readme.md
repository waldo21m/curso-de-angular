# Curso de Angular

Prof. Nicolas Molina.

## Lecciones:

- [Introducción a Angular](#module01)
  - [Lección #1 - Todo lo que aprenderás sobre Angular](#lesson01)
  - [Lección #2 - Instalación y preparación del entorno de desarrollo](#lesson02)
  - [Lección #3 - Preparando el entorno para Windows](#lesson03)
  - [Lección #4 - Preparando el entorno para Linux](#lesson04)
  - [Lección #5 - Crea tu primera aplicación en Angular](#lesson05)
- [Estructuras de control](#module02)
  - [Lección #6 - Introducción al Angular CLI y proyecto del curso](#lesson06)
  - [Lección #7 - String interpolation](#lesson07)
  - [Lección #8 - Data biding en Angular](#lesson08)
  - [Lección #9 - Uso de ngIf](#lesson09)
  - [Lección #10 - Uso de ngFor add y delete](#lesson10)
  - [Lección #11 - Uso de ngFor para recorrer objetos](#lesson11)
  - [Lección #12 - Uso de ngSwitch](#lesson12)
- [Componentes](#module03)
  - [Lección #13 - ¿Qué son los componentes y decoradores?](#lesson13)
  - [Lección #14 - Uso de Inputs y Outputs](#lesson14)
  - [Lección #15 - Ciclo de vida de los componentes](#lesson15)
  - [Lección #16 - Estilos para mostrar la lista de productos](#lesson16)
  - [Lección #17 - Uso de ng generate y ng lint](#lesson17)
- [Pipes y Directivas](#module04)
  - [Lección #18 - Usando los pipes de Angular](#lesson18)
  - [Lección #19 - Construyendo un propio pipe](#lesson19)
  - [Lección #20 - Construyendo una directiva propia](#lesson20)
- [Módulos y Rutas](#module05)
  - [Lección #21 - Introducción al NgModule](#lesson21)
  - [Lección #22 - Creando rutas en Angular](#lesson22)
  - [Lección #23 - Creando la página home de la tienda](#lesson23)
  - [Lección #24 - Usando routerLink y routerActive](#lesson24)
  - [Lección #25 - Creando el detalle de cada producto](#lesson25)
  - [Lección #26 - Elaboración de la página de detalle de producto](#lesson26)
  - [Lección #27 - Creando el módulo del website con vistas anidadas](#lesson27)
  - [Lección #28 - Preparar el proyecto para Lazy Loading](#lesson28)
  - [Lección #29 - Implementación del Lazy Loading](#lesson29)
  - [Lección #30 - Creando un shared module y core module](#lesson30)
  - [Lección #31 - Guardianes](#lesson31)
- [UI/UX, estilos](#module06)
  - [Lección #32 - Instalando Angular Material](#lesson32)
  - [Lección #33 - Instalando un sistema de grillas](#lesson33)
  - [Lección #34 - Creando el header](#lesson34)
  - [Lección #35 - Estilos a product-card](#lesson35)
  - [Lección #36 - Creando vistas con Angular schematic](#lesson36)
- [Servicios](#module07)
  - [Lección #37 - Creando nuestros propios servicios: HTTP Client](#lesson37)
  - [Lección #38 - Haciendo una solicitud GET desde el servicio](#lesson38)
  - [Lección #39 - Haciendo una solicitud POST desde el servicio](#lesson39)
  - [Lección #40 - Haciendo una solicitud PUT y DELETE desde el servicio](#lesson40)
  - [Lección #41 - Ambientes en Angular](#lesson41)
  - [Lección #42 - Lista de inventario y detalle](#lesson42)
  - [Lección #43 - Cierre de ejercicio de detalle](#lesson43)
- [Formularios](#module08)
  - [Lección #44 - Introducción al FormControl](#lesson44)
  - [Lección #45 - Creando el formulario de productos](#lesson45)
  - [Lección #46 - Ajustar estilos en un formulario](#lesson46)
  - [Lección #47 - Validaciones personalizadas](#lesson47)
  - [Lección #48 - Editar un producto a través de un formulario](#lesson48)
- [Programación reactiva](#module09)
  - [Lección #49 - Añadiendo productos al carrito](#lesson49)
  - [Lección #50 - Usa un pipe para hacer un contador de productos](#lesson50)
  - [Lección #51 - Creando la página de la orden y uso de async](#lesson51)
- [Firebase](#module10)
  - [Lección #52 - Instalar Angular Firebase y configurar Firebase Auth](#lesson52)
  - [Lección #53 - Implementando Auth y Guards](#lesson53)
  - [Lección #54 - Subiendo una imagen a Firebase Storage](#lesson54)
  - [Lección #55 - Haciendo deploy a Firebase Hosting](#lesson55)
- [Conclusiones](#module11)
  - [Lección #56 - Repaso del curso](#lesson56)

## <a name="module01"></a> Introducción a Angular

### <a name="lesson01"></a> Lección #1 - Todo lo que aprenderás sobre Angular

---

Hola. Quiero darte la bienvenida al curso de Angular.

Yo soy Nicolás Molina y soy Google Developer Expert en la categoría de Angular y tecnologías web. Me puedes encontrar en Twitter como @nicobytes y llevo más de ocho años de experiencia trabajando con este framework y lo conozco desde que era Angular.js

El objetivo de este curso es que tú logres crear aplicaciones de alto nivel con buenas prácticas y utilizando todos los artefactos, utilizando todo el ecosistema que alrededor de él hasta llevar aplicaciones a producción. Pero... ¿sabes qué es Angular? En este punto Angular se conoce más como una plataforma con el cual puedes desarrollar aplicaciones web progresivas, a utilizar formularios reactivos, hacer testing, utilizar el sistema de diseño Material Design para diseñar sus aplicaciones.

Angular es utilizado en este momento por miles de empresas como Google y como la NBA. Ahora que ya sabes un poco sobre Angular y sobre mi, acompáñame en la siguiente clase a preparar tu entorno de desarrollo para este curso.

### <a name="lesson02"></a> Lección #2 - Instalación y preparación del entorno de desarrollo

---

Para este curso se necesita NodeJs en su versión > 12 y NPM > 6.

También, podemos trabajar con VSCode e instalar el paquete **Angular Language Service y TSLint de Microsoft**.

Por último, instalamos el CLI de Angular con el comando `npm i -g @angular/cli`.

### <a name="lesson03"></a> Lección #3 - Preparando el entorno para Windows

---

Preparación del entorno de desarrollo en Windows.

### <a name="lesson04"></a> Lección #4 - Preparando el entorno para Linux

---

Preparación del entorno de desarrollo en Linux.

### <a name="lesson05"></a> Lección #5 - Crea tu primera aplicación en Angular

---

Para esta lección, ejecutaremos el comando `ng new waldo21m-store --directory ./`. Para este curso, activaremos el Angular routing y usaremos como preprocesador de estilos a SCSS.

Para correr el proyecto, ejecutamos el comando `ng serve`.

Iremos profundizando en los archivos en las futuras lecciones pero algunos que vemos acá son:

- angular.json: Tiene las configuraciones de nuestro proyecto de Angular.
- src: Donde tendremos nuestro proyecto de Angular.
- src/app: Ahí tendremos la configuración inicial con los archivos `app.component.html` y `app.module.ts`.

## <a name="module02"></a> Estructuras de control

### <a name="lesson06"></a> Lección #6 - Introducción al Angular CLI y proyecto del curso

---

Para esta lección, nos apoyaremos de la [documentación](https://angular.io/cli) y ejecutaremos el comando `ng --version`. Con este comando podemos ver que paquetes y en que versión se encuentran instalados en nuestro proyecto.

_También lo podemos ver con el package.json._

El comando `ng serve` corremos la aplicación... Si necesitamos ayuda recuerda que siempre podemos ejecutar el flag --help para ver el resto de comandos o nos podemos apoyar con la [documentación](https://angular.io/cli/serve). Para ver nuestra app corriendo, accederemos a http://localhost:4200/ que es el puerto por defecto de Angular.

Con el comando `ng build --prod` compilamos nuestra aplicación donde creará una carpeta llamada **dist/**, el cual podemos moverlo a un ambiente productivo o algún servidor de archivos estáticos.

### <a name="lesson07"></a> Lección #7 - String interpolation

---

En esta lección, borraremos el contenido de `src/app.component.html`.

El string interpolation `{{ 1 + 2 }}` nos permite ejecutar código valido de JavaScript en páginas HMTL (Similar a React o Vue).

Una vista o un componente en Angular se compone de un html, un archivo css y de un archivo que se encarga de su lógica. Ejemplo:

- app.component.html.
- app.component.ts.
- app.component.scss.

De ese archivo .ts, podremos crear variables, definir el template, etc. Por último, usaremos la variable title que viene por defecto en el `app.component.ts`.

### <a name="lesson08"></a> Lección #8 - Data biding en Angular

---

El string interpolation hace un enlace de datos entre nuestro componente hasta nuestro template. Esto, técnicamente se le conoce como enlace de datos o Data Biding.

En esta lección, usaremos la directiva **ngModel** en una etiqueta input. Pero, para que pueda ser usado correctamente, debemos registrarlo en el archivo `app.module.ts` e importarlo en el NgModule.

### <a name="lesson09"></a> Lección #9 - Uso de ngIf

---

Es una directiva directa de Angular que nos permite hacer condicionales en nuestro template.

### <a name="lesson10"></a> Lección #10 - Uso de ngFor add y delete

---

Es una directiva que nos permite recorrer arreglos. También, crearemos dos métodos que nos permitirá agregar o eliminar items de un elemento li.

### <a name="lesson11"></a> Lección #11 - Uso de ngFor para recorrer objetos

---

En esta lección, recorreremos un array de objetos de productos.

**Nota: Cuando definimos un atributo como [src]="product.image", no hace falta el string interpolation para acceder a las variables... Ya Angular interpreta de que vamos a acceder a una variable, de lo contrario, sería algo como src={{ product.image }}.**

Una buena práctica es tipar nuestros arrays como lo hicimos en esta lección.

### <a name="lesson12"></a> Lección #12 - Uso de ngSwitch

---

Con esta directiva, podemos crear múltiples ngIf de una forma más sencilla. Es muy similar al switch de toda la vida.

## <a name="module03"></a> Componentes

### <a name="lesson13"></a> Lección #13 - ¿Qué son los componentes y decoradores?

---

Un componente es un elemento independiente y reutilizable.

Angular trabaja con decoradores. Un decorador Angular es una clase especial de declaración que puede acoplarse a una clase, método, propiedad o parámetro. Los decoradores se declaran de la siguiente manera **@expression**. Gracias al parámetro `@`, podemos reconocer fácilmente que estamos hablando de un decorador. Generalmente usamos los decoradores para extender servicios de terceros, de esta manera evitamos modificar el código original del módulo y en tiempo de ejecución agregamos el funcionamiento que necesitamos.

### <a name="lesson14"></a> Lección #14 - Uso de Inputs y Outputs

---

En este clase vamos hablar sobre input y output, que es la manera en que podemos enviar datos a un componente y también recibir datos de él (Muy similar a los props de React).

A esto, técnicamente en Angular, se le conoce como property binding y event binding donde pues podemos enviar por medio de una propiedad datos y luego recibirlas por medio de eventos a lo cual vamos a llamar input y outputs.

**Importante: Los problemas de variables sin inicializar lo resolveremos con Type Assertions.**

En esta clase vemos dos detalles muy importantes. Los corchetes [] representan a un Input, mientras que los parentesis () representan a un Output. Los Outputs son útiles cuando queremos retornar elementos de un componente hijo al padre mientras que los corchetes se asemejan a un Prop de React... Es decir, sirven para recibir argumentos.

Gracias a ello, se aplican los principios Solid a un componente donde solo debe cumplir una tarea en específico.

### <a name="lesson15"></a> Lección #15 - Ciclo de vida de los componentes

---

Es importante entender el ciclo de vida de un componente desde que es creado, puesto en interfaz hasta que es eliminada de la misma. Angular tiene unos eventos que ejecuta en un orden en específico haciendo el ciclo de vida de un componente. 

El Primero de ellos es el constructor, que es el que él utiliza para crear nuestro componente y ponerle un interfaz.

Luego está ngOnChange que lo que haces detecta el cambio cada vez que tenemos un input. Nosotros ya vimos sobre inputs y vemos la forma de enviarle data ngOnChange es la forma en que cada vez que cambiamos esa data podemos detectar estos cambios y del estado anterior y el estado nuevo. 

Ahora tenemos ngOnInit que se ejecuta solo una vez, a diferencia de ngOnChange que se ejecuta varias veces cada que cambiamos un input, pero ngOnInit se ejecuta sólo una vez, cuando el componente ya está listo en interfaz gráfica. Aquí es buena idea hacer llamadas a componentes. Por ejemplo, hacer la llamada hacia una REST API.

Luego está ngDoCheck, que a diferencia lo que detecta es cuando los elementos hijos de ese componente También se han creado y son puestos y un interfaz.

Tenemos otros submétodos que luego vamos a ver más en específico.

Finalmente tenemos ngOnDestroy que simplemente detecta cuando elemento es quitado desde la interfaz.

**Importante: ngOnChanges y ngDoCheck tienen un error de colisión, ya que los dos pueden cumplir la tarea de escuchar por cambios del componente. El primero es la forma nativa de Angular, el segundo es una forma customizada para ello.**

ngOnDestroy nos ayudará remover las suscripciones a datos que inicializamos en algún momento, así podemos evitar bucles y fugas de memoria en la aplicación, es decir, limpiamos procesos de memoria siguiendo buenas prácticas.

### <a name="lesson16"></a> Lección #16 - Estilos para mostrar la lista de productos

---

En esta lección, vamos a darle estilos al componente de productos. Para ello, desde el decorador @Component agregaremos el styleUrls. Podemos usar más de una hoja de estilos pero se recomienda solo usar una.

Los estilos de forma global se crean en `src/styles.scss`.

El pseudónimo `:host` hace referencia a la etiqueta con el nombre del componente... En este caso _app-product_. Los estilos que se encuentran en un componente no afectará los estilos de otros componentes.

### <a name="lesson17"></a> Lección #17 - Uso de ng generate y ng lint

---

El comando `ng generate` o simplemente `ng g`, nos permite generar y/o modificar archivos basados en un esquema. En esta lección, crearemos un CartComponent con el comando: `ng g c cart`.

Esto nos agiliza el trabajo a la hora de crear y registrar componentes.

También, podemos ejecutar el linter de Angular con el comando `ng lint`. Si el paquete no está agregado, se puede hacer con el comando: `ng add @angular-eslint/schematics`.

https://angular.io/cli/generate

## <a name="module04"></a> Pipes y Directivas

### <a name="lesson18"></a> Lección #18 - Usando los pipes de Angular

---

En este módulo vas a aprender acerca de pipes y directivas que son dos artefactos muy utilizados a la hora de construir aplicaciones en Angular. Los pipes funcionan como una tubería donde entran datos de un tipo y los transformas entregándole a otro pipe otro tipo de valor. 

Sin embargo, los pipes se pueden utilizar de forma concatenada como una tubería o puedes hacer uso de un solo pipe. Angular ya tiene construidos varios pipes por defecto que los vamos a utilizar justo ahora.

Ejemplo: `{{ product.title | uppercase }}` o `{{ product.description | slice:0:10 }}`

Podemos profundizar más de pipes en: https://angular.io/api/common/DatePipe

### <a name="lesson19"></a> Lección #19 - Construyendo un propio pipe

---

Para ello, podemos usar el comando `ng g p exponential` para crear nuestro pipe personal. Podríamos vernos tentados el crear funciones en los componentes para hacer estas transformaciones... Sin embargo, eso nos penalizaría en rendimiento de la App.

### <a name="lesson20"></a> Lección #20 - Construyendo una directiva propia

---

Las directivas de Angular son básicamente funciones que son invocadas cuando el DOM (Document Object Model) es compilado por el framework de Angular. Se podría decir que las directivas están ligadas a sus correspondientes elementos del DOM cuando el documento es cargado. La finalidad de una directiva es modificar o crear un comportamiento totalmente nuevo.

Para generar una directiva lo hacemos con el comando: `ng g d highlight`.

Seguro te surge una pregunta y es esto lo habría podido hacer directamente con unas clases en CSS. Sin embargo, las directivas están para modificar el comportamiento del DOM dinámicamente. Debemos tener mucho cuidado en donde y cómo lo utilizamos, ya que no es buena práctica manipular el DOM dinámicamente porque para eso tenemos el data binding de Angular. Sin Embargo, sí podría ser muy funcional, por ejemplo, para hacer un componente de tipo autocompletar, donde por cada tecla que va escribiendo en el input vamos modificando el DOM y añadiendo nuevos elementos.

## <a name="module05"></a> Módulos y Rutas

### <a name="lesson21"></a> Lección #21 - Introducción al NgModule

---

En este bloque manejaremos módulos y rutas.

Los módulos y rutas nos sirven para poder abstraer y dividir mejor por dominio nuestra aplicación. Todo lo que hemos hecho hasta ahora como pipes, componentes y directivas, lo hemos ingresado a un solo modulo, el módulo de app.module. En teoría está bien por como un módulo, lo que haces resumir o adjuntar varios artefactos de Angular como servicios componentes directivas.

Sin embargo, lo estamos metiendo todo en un solo módulo. Esto es una mala práctica. Podemos modularizar nuestra aplicación por dominio. 

La idea es modularizarlo por carpeta como `src/app/about-us`, `src/app/contact`, etc. También tenemos modulos especiales. Los módulos especiales dentro de Angular son dos: El módulo core y el modulo shared. 

El módulo Core guarda todos los servicios o todos los componentes que vamos a utilizar en todos los otros módulos. Eso quiere decir que genera una sola referencia.

Recuerda que entre los principios Solid está el principio Singleton que lo que haces guardar una referencia solo en nuestras cosas. Por ejemplo, aquí peculiarmente podríamos tener un servicio de autentificación, ya que en una aplicación sólo tenemos un servicio que maneja la autentificación de nuestros usuarios. 

Sin embargo, en el módulo Shared podemos tener componentes y servicios compartidos. Por ejemplo, si yo tengo un componente que es utilizado por varios modulos, lo podría meter en el módulo shared. Esto puede sonar teórico en este momento, pero vamos a verlo en la práctica.

### <a name="lesson22"></a> Lección #22 - Creando rutas en Angular

---

En esta lección vamos a usar múltiples componentes usando las rutas de Angular. Crearemos los módulos contact, demo, home y products.

### <a name="lesson23"></a> Lección #23 - Creando la página home de la tienda

---

En esta lección trabajaremos en el home. Crearemos un componente header, banner y footer y los utilizaremos en el home. También usaremos slider [Swiper](https://swiperjs.com/) para el banner. Para instalarlo leemos la documentación y seguiremos las instrucciones... En este caso, se debe ejecutar el comando `npm i swiper --save`.

Además de instalarlo, debemos registrarlo en nuestro archivo angular.json.

**Importante: Es mejor que nos basemos en los ejemplos que están en la página de Swiper y no hacerlo como en esta lección. Incluso no hace falta registrarlo en angular.json sino que debemos importar los estilos en `styles.scss`.**

### <a name="lesson24"></a> Lección #24 - Usando routerLink y routerActive

---

En esta lección, crearemos el header para hacer una barra de navegación.

Cuando trabajamos con Angular, React o Vue automáticamente estamos construyendo una SPA. Para movernos entre vistas usamos la propiedad routerLink en un href. Otro elemento que veremos es el routerLinkActive que simplemente es la ruta de un elemento que está actualmente activo.

También en esta lección, veremos como redireccionar de una página a otra y crear un error 404. El doble `**` significa que no hubo match en la ruta.

### <a name="lesson25"></a> Lección #25 - Creando el detalle de cada producto

---

En esta lección crearemos nuestro primer servicio con el comando: `ng g s products`, el cual se encargará de mostrar el listado de productos y el detalle de un producto en particular.

Una **inyección de dependencias** es un patrón de diseño orientado a objetos, en el que se suministran objetos a una clase en lugar de ser la propia clase la que cree dichos objetos. Este concepto es muy importante porque estaremos inyectando objetos a los componentes (como los servicios y las rutas) para poder interactuar con sus métodos.

### <a name="lesson26"></a> Lección #26 - Elaboración de la página de detalle de producto

---

Lección practica para repasar todo lo visto en el curso.

### <a name="lesson27"></a> Lección #27 - Creando el módulo del website con vistas anidadas

---

En esta clase ahora vas a aprender una técnica de reutilización de código que Angular te provee con el router. A esto se le llama vistas anidadas. Lo que haremos es crear un componente Layout que tendrá el header y el footer.

### <a name="lesson28"></a> Lección #28 - Preparar el proyecto para Lazy Loading

---

Lazy Loading es una técnica de Angular que te va a ayudar a reducir el peso de tus aplicaciones o al menos a fragmentarlo para reducir el tiempo de carga inicial y hacer que tus aplicaciones carguen más rápido.

La técnica consiste en crear módulos en nuestra aplicación, muy similar al `app.module.ts`. En esta lección vamos a preparar los archivos y en la siguiente se realizará la implementación.

### <a name="lesson29"></a> Lección #29 - Implementación del Lazy Loading

---

Terminaremos de implementar el Lazy loading... Para ello, debemos mover nuestro componente antiguo a la carpeta components del módulo para seguir las buenas prácticas. Registraremos el modulo en el `app-routing` y usaremos el SwiperModule en el HomeModule y no en el AppModule.

Además, ejecutaremos el preloadingStrategy (en el app-routing) que consiste en precargar los diversos módulos **solo cuando el navegador se encuentre libre**. Esto es muy útil para las redes lentas pero que ya tienen en cache los distintos archivos de nuestra aplicación.

### <a name="lesson30"></a> Lección #30 - Creando un shared module y core module

---

Ya aprendimos a hacer una modularización de una página o de un elemento en particular, creando un módulo, simplemente que contenga los aspectos como componentes, directivas y pipes, y cargarlo con la técnica de lazy loading.

Un componente que también podemos modularizar son los elementos compartidos. En este momento, en la página de app.module se compartiendo varios elementos entre si. Por ejemplo, el header, el footer son elementos que son constantes y que los estamos utilizando. A estos elementos que se pueden compartir entre sí, se le llaman o se le puede básicamente poner dentro de un shared module o módulo compartido.

También, generaremos un módulo core. El core module, lo único que hace es se segmentar o también agrupar componentes si y solo si se van a compartir a través de toda la aplicación, pero sólo generando una referencia única. Esa es la única diferencia con el shared module. Miren que conceptualmente se parecen, es decir, los dos comparten, solo que el shared module tiene que ser importado y el core module por defecto, va a estar en todos los módulos, lo importes o no, simplemente va a estar.

Entonces el core module sirve sobre todo para guardar servicios, servicios que tengan una sola referencia de los datos. Entonces es una buena práctica en el core module meter los servicios de datos y en shared module, meter todo lo que sean componentes directivas y/o pipes. Como que todas las cuestiones de artefactos gráficos los podemos meter en el shared module y todos los que sean servicios de datos los podemos meter en el core. Esa es una buena práctica.

**Notas:** 

Para generar un módulo, en las lecciones anteriores lo hicimos manualmente... Pero, podemos crearlo ejecutando el comando: `ng g m shared`.

### <a name="lesson31"></a> Lección #31 - Guardianes

---

La seguridad es importante en cualquier tipo de aplicación y Angular te brinda guardianes para poder saber quién puede entrar o no a una ruta en específico.

A esto Angular oficialmente le llama guardianes de varios tipos de guardianes. Adivina, con el generador de Angular podríamos generar un guardián desde la línea de comandos. Vamos a generarlo con el comando: `ng g g`, doble g, la primera es de generate y la otra de guard.

Y vamos a generar uno que diga admin. Existen diversos tipos de guardianes pero en esta lección usaremos el _CanActive_ que consiste simplemente en activar o desactivar una ruta. Para esta lección, simplemente desactivaremos la ruta de contact.

## <a name="module06"></a> UI/UX, estilos

### <a name="lesson32"></a> Lección #32 - Instalando Angular Material

---

Para esta lección, instalaremos [Material Angular UI](https://material.angular.io/) con el comando: `ng add @angular/material`. Material nos hará una serie de preguntas:

1. La primera es que tipo de tema usaremos: En este curso usaremos Indigo/Pink.
2. La segunda pregunta es si deseamos setear de forma global la tipografía de Material Angular: En este caso si.
3. Establecer las animaciones para Angular Material: Si.

Y se seguirá instalando.

**Importante: Es mejor seguir las instrucciones de Material Angular UI para instalarlo de forma correcta. Una vez instalado, debemos correr el servidor nuevamente para que agarre los estilos de forma correcta.**

Para usarlo, una sugerencia es crear un módulo compartido llamado material donde podamos usarlo en cualquier parte del proyecto con el comando: `ng g m material`. Otra opción es importar simplemente los módulos que necesitemos en los componentes que haga falta.

### <a name="lesson33"></a> Lección #33 - Instalando un sistema de grillas

---

En esta clase vamos a instalar un sistema de grillas usando Flex box. Angular Material tiene un sistema de grillas basado en CSS Grid pero lo más normal dentro de una aplicación es que usemos Flexbox para construir elementos. 

Ahora te invito a utilizar una librería muy común para esto que es Flexbox Grid que es muy liviano y nos sirve precisamente para resolver este problema.

Para instalarlo, ejecutamos el comando: `npm i flexboxgrid --save`.

Una vez instalado, debemos registrar el css en nuestro angular.json y ejecutar nuevamente el servidor.

### <a name="lesson34"></a> Lección #34 - Creando el header

---

En esta clase, vamos a desarrollar el header utilizando toolbar de material design y flexbox.

Iconos de Material Design: https://fonts.google.com/icons?selected=Material+Icons

### <a name="lesson35"></a> Lección #35 - Estilos a product-card

---

Para esta lección, vamos a darle estilos al product usando Cards y CSS.

### <a name="lesson36"></a> Lección #36 - Creando vistas con Angular schematic

---

Ahora con Angular schematic puedes generar interfaces bastante potentes con solo una linea de código generada desde la terminal.

Para lograr esto, vamos a crear un módulo llamado admin module donde vamos a ingresar componentes hacia este módulo, para que todo quede modularizado y organizado.

Para generar este módulo, lo único que tenemos que hacer es digitar el comando: `ng g m` y luego le decimos el nombre de nuestro módulo. Si quieres que este módulo automáticamente te genere el módulo de rutas sin tener que echar ninguna línea de código, podemos darle la bandera "routing", con esto que nos va a crear un módulo que, a pesar de todo ya tiene un routing incorporado. Ejemplo: `ng g m admin --routing`.

Para usar Angular Schematics, debemos seguir los pasos de [la documentación](https://material.angular.io/guide/schematics).

Crearemos un schematic del tipo address-form de la siguiente manera: `ng generate @angular/material:address-form admin/components/product-form`.

Otro schematic que crearemos es el siguiente: `ng generate @angular/material:navigation admin/components/nav`.

## <a name="module07"></a> Servicios

### <a name="lesson37"></a> Lección #37 - Creando nuestros propios servicios: HTTP Client

---

Para este módulo, dejaremos de usar los Mockup y nos enlazaremos a https://platzi-store.herokuapp.com/products para consumir los servicios mediante REST API.

Lo que debemos hacer es usar el módulo de HttpClientModule en el app.module.ts para poder consumir servicios.

En esta lección, dejaremos funcionando el listado de productos... Mas no el detalle.

### <a name="lesson38"></a> Lección #38 - Haciendo una solicitud GET desde el servicio

---

En esta lección, haremos funcionar el detalle de productos. Además, el endpoint lo vamos a manejar en una variable de entorno.

### <a name="lesson39"></a> Lección #39 - Haciendo una solicitud POST desde el servicio

---

 Para crear productos, usamos la ruta https://platzi-store.herokuapp.com/products/ con el verbo POST. Este recibe el siguiente objeto:
 
 ```json
{
  "id": "1",
  "title": "Camiseta",
  "image": "assets/images/camiseta.png",
  "price": 80000,
  "description": "bla bla bla bla bla"
}
```

### <a name="lesson40"></a> Lección #40 - Haciendo una solicitud PUT y DELETE desde el servicio

---

En esta lección, editaremos y actualizaremos registros en Angular.

Si queremos recibir de forma parcial un objeto, lo hacemos mediante Ts con `Partial<MyObject>`

### <a name="lesson41"></a> Lección #41 - Ambientes en Angular

---

Manejo de ambientes de desarrollo con Angular. Si ejecutamos el comando `ng build --prod` podemos comprobar si compila nuestro programa para producción o no. Si no compila, la terminal nos irá indicando la clase de errores que tenemos en la app.

Además, crearemos un ambiente staging que debemos registrar en el angular.json. Después, se debe ejecutar el comando `ng serve -c=stag`.

Se puede ejecutar como `ng serve` o podemos usarlo también cuando deseamos compilar usando el comando: `ng build -c=stag`.

### <a name="lesson42"></a> Lección #42 - Lista de inventario y detalle

---

En esta lección, crearemos el listado de productos y detalles del administrador. Para ello, crearemos un componente con el siguiente comando: `ng g c admin/components/products-list`.

En la siguiente lección se terminará este ejercicio.

### <a name="lesson43"></a> Lección #43 - Cierre de ejercicio de detalle

---

Finalización del listado de detalles e implementación del endpoint de eliminar.

## <a name="module08"></a> Formularios

### <a name="lesson44"></a> Lección #44 - Introducción al FormControl

---

Los formularios es la forma más habitual de capturar datos en un sistema.

Cualquier tipo de sistema sea móvil o web, la forma normal en que capturamos datos de un usuario es mediante un formulario y esta no va a ser la excepción. Necesitamos uno para crear el producto, un formulario para que nuestro usuario ingrese su email y poderlo contactar.

Vamos a crear un contacto, un campo de email para que nos deje su correo y poder le enviar correos de promociones.

¿Cómo empezamos a manejar este tipo de campos con Angular? La mejor manera es controlarlos con un form control. Para esto tenemos que utilizar formularios reactivos y los formularios reactivos son un módulo aparte que tenemos que incluir. Entonces vamos a decirle a nuestros shared module que aparte de incluir estos módulos de acá, incluya el componente o el módulo de formularios, pero formularios reactivos, ReactiveFormsModule.

Hasta ahora sólo habíamos trabajado con FormsModule, que es el ngModel del que habíamos trabajado anteriormente y en las clases iniciales, pero éstos son totalmente diferentes. Aquí tenemos formularios reactivos que son mucho más potentes que los que tiene el ngModel. Vamos simplemente a implementarlo. Ya con esto podemos utilizar formularios reactivos. 

¿Qué es un formulario reactivo? ¿Qué diferencia tienen con el FormsModule? La diferencia es sustancial y es que en un ngModel normal de los que estábamos generando, si tiene un enlace de datos, pero no podemos tener el control total, validar datos o hacer pruebas unitarias a él.

Con los formularios reactivos tenemos observables para poder mirar qué datos están mutando, colocarle validaciones, agregar pruebas unitarias y toda la lógica va a estar dentro de nuestro controlador. Por ende, hacer pruebas va a ser mucho más sencillo que hacerle pruebas al template.

Así que vamos a implementar nuestro primer form control.

### <a name="lesson45"></a> Lección #45 - Creando el formulario de productos

---

Llegó el momento de crear el formulario para administrar nuestros productos. Recuerda que un producto tiene el título, una descripción, un precio, una imagen.

Necesitamos desde el administrador crear un formulario para ingresar nuevos productos de nuestro sistema. Vamos a hacer un componente para lograrlo.

En la siguiente lección vamos a agregar estilos.

### <a name="lesson46"></a> Lección #46 - Ajustar estilos en un formulario

---

En esta clase ahora vamos a trabajar en los estilos de nuestro formulario para que ya se vea mejor y en el almacenamiento del producto.

### <a name="lesson47"></a> Lección #47 - Validaciones personalizadas

---

¿Cómo haríamos para hacer una validación que se salga de las reglas que ya nos propone Angular? Si viste, tenemos que Angular nos provee ya validaciones para indicar un Max o Min, es decir, que cumpla con el máximo un mínimo y hasta para validar un email. Pero ¿qué pasa si no necesitamos estas validaciones? ¿Podemos extenderlas? ¿Podemos crear las propias? Pues la respuesta es que sí y vamos a crearlas en este momento.

**Notas:**

La mejor forma de realizar validaciones personalizadas es creando una carpeta utils donde crearemos un archivo validators.ts. Las validaciones que crearemos, tendrá la misma estructura original de validators.

Otra buena práctica que se sugiere, es tratar a los campos del formulario mediante los getters nativos de Js/Ts. Así, no estaremos repitiendo código en el front y será mucho más legible.

### <a name="lesson48"></a> Lección #48 - Editar un producto a través de un formulario

---

En esta lección, crearemos un componente de la siguiente forma: `ng g c admin/components/product-edit`.

Será muy similar al formulario de creación solo que quitaremos el id del formulario y haremos uso de `this.form.patchValue({...values})` para rellenar el formulario cuando se consulte la información.

## <a name="module09"></a> Programación reactiva

### <a name="lesson49"></a> Lección #49 - Añadiendo productos al carrito

---

La **programación reactiva** es un paradigma enfocado en el trabajo con flujos de datos finitos o infinitos de manera asíncrona.

La programación reactiva es la forma de conntrolar flujos de datos efectivamente en tus componentes Angular. En este proyecto de nuestra tienda tenemos agregar productos al carrito y esto está obteniéndose automáticamente desde una REST API que viene la información desde una base de datos.
 
¿Cómo lograríamos que dinámicamente cuando agregamos un producto este reaccione automáticamente? Esto lo hacemos con programación reactiva.

Para ello, crearemos un servicio cart con el comando: `ng g s core/services/cart`.

Usaremos la librería rxjs que añade principios reactivos a nuestra aplicación en Angular.

### <a name="lesson50"></a> Lección #50 - Usa un pipe para hacer un contador de productos

---

En la lección pasada, implementamos la funcionalidad agregar productos al carrito mediante programación reactiva. Sin embargo, podemos mejorar el contador usando pipes.

Básicamente, lo que haremos es dejar de suscribirnos en el component.ts y crearemos un observable nuevo llamado total$. Haremos uso de map de los operators de rxjs para usar un pipe en el observable del cart, que nos retornará la cantidad de productos que hay en el carrito, mientras que el observable total$, podremos usarlo directamente en el component.html usando el pipe async.

Esto, va a hacer automáticamente la suscripción y cuando el elemento ya no se esté utilizando, va a desuscribirse, evitando flujos o pérdidas de memoria o simplemente teniendo un mejor performance.

### <a name="lesson51"></a> Lección #51 - Creando la página de la orden y uso de async

---

Para esta lección, crearemos el módulo de ordenes (Del lado público). Para ello, ejecutamos el comando `ng g m order --routing`. Posteriormente, vamos a crear la página de order con el comando `ng g c order/components/order`.

Esta lección es un repaso de programación reactiva junto con las lecciones 27 en adelante que consiste en la creación de módulos.

**Reto: Agrupar los elementos repetidos en order usando un pipe.**

## <a name="module10"></a> Firebase

### <a name="lesson52"></a> Lección #52 - Instalar Angular Firebase y configurar Firebase Auth

---

### <a name="lesson53"></a> Lección #53 - Implementando Auth y Guards

---

### <a name="lesson54"></a> Lección #54 - Subiendo una imagen a Firebase Storage

---

### <a name="lesson55"></a> Lección #55 - Haciendo deploy a Firebase Hosting

---

## <a name="module11"></a> Conclusiones

### <a name="lesson56"></a> Lección #56 - Repaso del curso

---
