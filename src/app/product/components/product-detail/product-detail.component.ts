import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';

import { ProductsService } from '../../../core/services/products/products.service';
import { Product } from "../../../models/product.model";

@Component({
  selector: 'app-product-detail',
  templateUrl: './product-detail.component.html',
  styleUrls: ['./product-detail.component.scss']
})
export class ProductDetailComponent implements OnInit {

  product: Product = {} as Product;

  constructor(
    private route: ActivatedRoute,
    private productsService: ProductsService,
  ) { }

  ngOnInit(): void {
    this.route.params.subscribe((params: Params) => {
      const id = params.id;
      this.fetchProduct(id);
    });
  }

  fetchProduct(id: string) {
    this.productsService.getProduct(id).subscribe((product) => {
      this.product = product;
    })
  }

  createProduct() {
    const newProduct: Product = {
      id: '3263',
      title: 'Nuevo desde Angular',
      image: 'assets/images/404.png',
      price: 3000,
      description: 'Nuevo producto'
    };
    this.productsService.createProduct(newProduct).subscribe((product) => {
      console.log(product);
    })
  }

  updateProduct() {
    const updateProduct: Partial<Product> = {
      price: 3001,
      description: 'Holis'
    };
    this.productsService.updateProduct('3263', updateProduct).subscribe((product) => {
      console.log(product);
    });
  }

  deleteProduct() {
    this.productsService.deleteProduct('3263').subscribe((response) => {
      console.log(response);
    });
  }
}
