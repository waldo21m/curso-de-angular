import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { ActivatedRoute, Params, Router } from "@angular/router";
import { MyValidators } from "../../../utils/validators";

import { Product } from "../../../models/product.model";

import { ProductsService } from "../../../core/services/products/products.service";

@Component({
  selector: 'app-product-edit',
  templateUrl: './product-edit.component.html',
  styleUrls: ['./product-edit.component.scss']
})
export class ProductEditComponent implements OnInit {

  id = "";
  form: FormGroup = this.formBuilder.group({
    title: [null, [Validators.required]],
    price: [null, [Validators.required, MyValidators.isPriceValid]],
    image: [null],
    description: [null, [Validators.required]],
  });

  constructor(
    private formBuilder: FormBuilder,
    private productsService: ProductsService,
    private router: Router,
    private activeRoute: ActivatedRoute,
  ) { }

  ngOnInit(): void {
    this.activeRoute.params.subscribe((params: Params) => {
      this.id = params.id;
      this.productsService.getProduct(this.id).subscribe((product: Product) => {
        this.form.patchValue(product);
      });
    });
  }

  updateProduct(event: Event) {
    event.preventDefault();
    if (this.form.valid) {
      const product = this.form.value;
      this.productsService.updateProduct(this.id, product).subscribe((updatedProduct) => {
        console.log(updatedProduct);
        this.router.navigate(['./admin/products']);
      });
    }
  }

  get priceField() {
    return this.form.get('price');
  }

}
