import { Component, OnInit } from '@angular/core';

import { Product } from "../../../models/product.model";

export interface Order {
  id: string;
  product: Product;
  quantity: number;
  total: number;
}

const ordersList: Order[] = [
  {
    id: '1',
    product: {
      id: '1',
      image: 'assets/images/camiseta.png',
      title: 'Camiseta',
      price: 80000,
      description: 'bla bla bla bla bla',
    },
    quantity: 1,
    total: 80000,
  },
  {
    id: '2',
    product: {
      id: '1',
      image: 'assets/images/camiseta.png',
      title: 'Camiseta',
      price: 80000,
      description: 'bla bla bla bla bla',
    },
    quantity: 1,
    total: 80000,
  },
  {
    id: '3',
    product: {
      id: '3',
      image: 'assets/images/mug.png',
      title: 'Mug',
      price: 80000,
      description: 'bla bla bla bla bla',
    },
    quantity: 1,
    total: 80000,
  },
  {
    id: '4',
    product: {
      id: '5',
      image: 'assets/images/stickers1.png',
      title: 'Stickers',
      price: 80000,
      description: 'bla bla bla bla bla',
    },
    quantity: 1,
    total: 80000,
  },
];

@Component({
  selector: 'app-orders-list',
  templateUrl: './orders-list.component.html',
  styleUrls: ['./orders-list.component.scss']
})
export class OrdersListComponent implements OnInit {
  displayedColumns: string[] = ['id', 'product', 'quantity', 'total', 'actions'];
  orders = ordersList;

  constructor() { }

  ngOnInit(): void {
  }

  deleteOrder(id: string) {
    this.orders = this.orders.filter((order: Order) => {
      return order.id !== id;
    });
  }
}
